#
#  Be sure to run `pod spec lint ZSCategories.podspec' to ensure this is a
#  valid spec and to remove all comments including this before submitting the spec.
#
#  To learn more about Podspec attributes see http://docs.cocoapods.org/specification.html
#  To see working Podspecs in the CocoaPods repo see https://github.com/CocoaPods/Specs/
#

Pod::Spec.new do |s|

  # ―――  Spec Metadata  ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  These will help people to find your library, and whilst it
  #  can feel like a chore to fill in it's definitely to your advantage. The
  #  summary should be tweet-length, and the description more in depth.
  #

  s.name         = "ZSCategories"
  s.version      = "0.0.7"
  s.summary      = "项目中的类别管理"

  # This description is used to generate tags and improve search results.
  #   * Think: What does it do? Why did you write it? What is the focus?
  #   * Try to keep it short, snappy and to the point.
  #   * Write the description between the DESC delimiters below.
  #   * Finally, don't worry about the indent, CocoaPods strips it!
  s.description  = <<-DESC
  项目中的类别管理，包括YYCatories
                   DESC

  s.homepage     = "https://gitee.com/safiri"
  # s.screenshots  = "www.example.com/screenshots_1.gif", "www.example.com/screenshots_2.gif"


  # ―――  Spec License  ――――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  Licensing your code is important. See http://choosealicense.com for more info.
  #  CocoaPods will detect a license file if there is a named LICENSE*
  #  Popular ones are 'MIT', 'BSD' and 'Apache License, Version 2.0'.
  #

  # s.license      = "MIT (example)"
  s.license      = { :type => "MIT", :file => "LICENSE" }


  # ――― Author Metadata  ――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  Specify the authors of the library, with email addresses. Email addresses
  #  of the authors are extracted from the SCM log. E.g. $ git log. CocoaPods also
  #  accepts just a name if you'd rather not provide an email address.
  #
  #  Specify a social_media_url where others can refer to, for example a twitter
  #  profile URL.
  #

  s.author             = { "safiri" => "safiri@163.com" }
  # Or just: s.author    = "safiri"
  # s.authors            = { "safiri" => "safiri@163.com" }
  # s.social_media_url   = "http://twitter.com/safiri"

  # ――― Platform Specifics ――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  If this Pod runs only on iOS or OS X, then specify the platform and
  #  the deployment target. You can optionally include the target after the platform.
  #

  # s.platform     = :ios
  s.platform     = :ios, "5.0"

  #  When using multiple platforms
  # s.ios.deployment_target = "5.0"
  # s.osx.deployment_target = "10.7"
  # s.watchos.deployment_target = "2.0"
  # s.tvos.deployment_target = "9.0"


  # ――― Source Location ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  Specify the location from where the source should be retrieved.
  #  Supports git, hg, bzr, svn and HTTP.
  #

  s.source       = { :git => "https://gitee.com/safiri/ZSCategories.git", :tag => "#{s.version}" }


  # ――― Source Code ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  CocoaPods is smart about how it includes source code. For source files
  #  giving a folder will include any swift, h, m, mm, c & cpp files.
  #  For header files it will include any header in the folder.
  #  Not including the public_header_files will make all headers public.
  #

  s.source_files  = "Classes/ZSCategories/ZSCategories.h"
  # s.exclude_files = "Classes/Exclude"

  # s.public_header_files = "Classes/**/*.h"

  s.subspec 'YYCategories' do |yy|
  	yy.source_files = "Classes/ZSCategories/YYCategories/**/*.{h,m}"
  end

  # ----------------  Foundation ----------------
  s.subspec 'NSArray' do |nsArray|
  	nsArray.source_files = "Classes/ZSCategories/Foundation/NSArray/**/*.{h,m}"
  	nsArray.dependency 'ZSCategories/YYCategories'
  end

  s.subspec 'NSBundle' do |nsBundle|
  	nsBundle.source_files = "Classes/ZSCategories/Foundation/NSBundle/**/*.{h,m}"
  	nsBundle.dependency 'ZSCategories/YYCategories'
  	nsBundle.dependency 'ZSCategories/NSString'
  end

  s.subspec 'NSData' do |nsData|
  	nsData.source_files = "Classes/ZSCategories/Foundation/NSData/**/*.{h,m}"
  	nsData.dependency 'ZSCategories/YYCategories'
  end

  s.subspec 'NSDate' do |nsDate|
  	nsDate.source_files = "Classes/ZSCategories/Foundation/NSDate/**/*.{h,m}"
  	nsDate.dependency 'ZSCategories/YYCategories'
  end

  s.subspec 'NSDictionary' do |nsDictionary|
  	nsDictionary.source_files = "Classes/ZSCategories/Foundation/NSDictionary/**/*.{h,m}"
  	nsDictionary.dependency 'ZSCategories/YYCategories'
  end

  s.subspec 'NSNotificationCenter' do |nsNotificationCenter|
  	nsNotificationCenter.source_files = "Classes/ZSCategories/Foundation/NSNotificationCenter/**/*.{h,m}"
  	nsNotificationCenter.dependency 'ZSCategories/YYCategories'
  end

  s.subspec 'NSNumber' do |nsNumber|
  	nsNumber.source_files = "Classes/ZSCategories/Foundation/NSNumber/**/*.{h,m}"
  	nsNumber.dependency 'ZSCategories/YYCategories'
  end

  s.subspec 'NSObject' do |nsObject|
  	nsObject.source_files = "Classes/ZSCategories/Foundation/NSObject/**/*.{h,m}"
  	nsObject.dependency 'ZSCategories/YYCategories'
  end

  s.subspec 'NSString' do |nsString|
  	nsString.source_files = "Classes/ZSCategories/Foundation/NSString/**/*.{h,m}"
  	nsString.dependency 'ZSCategories/YYCategories'
  	nsString.dependency 'ZSCategories/NSData'
  	nsString.dependency 'ZSCategories/NSNumber'
  end

  s.subspec 'NSTimer' do |nsTimer|
  	nsTimer.source_files = "Classes/ZSCategories/Foundation/NSTimer/**/*.{h,m}"
  	nsTimer.dependency 'ZSCategories/YYCategories'
  end

  s.subspec 'NSUserDefaults' do |nsUserDefaults|
  	nsUserDefaults.source_files = "Classes/ZSCategories/Foundation/NSUserDefaults/**/*.{h,m}"
  	nsUserDefaults.dependency 'ZSCategories/YYCategories'
  end

  s.subspec 'Foundation' do |foundation|
  	foundation.dependency 'ZSCategories/NSArray'
  	foundation.dependency 'ZSCategories/NSBundle'
  	foundation.dependency 'ZSCategories/NSData'
  	foundation.dependency 'ZSCategories/NSDate'
  	foundation.dependency 'ZSCategories/NSDictionary'
  	foundation.dependency 'ZSCategories/NSNotificationCenter'
  	foundation.dependency 'ZSCategories/NSNumber'
  	foundation.dependency 'ZSCategories/NSObject'
  	foundation.dependency 'ZSCategories/NSString'
  	foundation.dependency 'ZSCategories/NSTimer'
  	foundation.dependency 'ZSCategories/NSUserDefaults'
  end

  # ----------------  Foundation ----------------


  # ----------------  UIKit ----------------

  s.subspec 'UIViewController' do |uisub|
  	uisub.source_files = "Classes/ZSCategories/UIKit/UIViewController/**/*.{h,m}"
  	uisub.dependency 'ZSCategories/YYCategories'
  end

  s.subspec 'UIApplication' do |uisub|
  	uisub.source_files = "Classes/ZSCategories/UIKit/UIApplication/**/*.{h,m}"
  	uisub.dependency 'ZSCategories/YYCategories'
  end

  s.subspec 'UIBezierPath' do |uisub|
  	uisub.source_files = "Classes/ZSCategories/UIKit/UIBezierPath/**/*.{h,m}"
  	uisub.dependency 'ZSCategories/YYCategories'
  	uisub.dependency 'ZSCategories/UIFont'
  end

  s.subspec 'UIButton' do |uisub|
  	uisub.source_files = "Classes/ZSCategories/UIKit/UIButton/**/*.{h,m}"
  	uisub.dependency 'ZSCategories/YYCategories'
  end

  s.subspec 'UIColor' do |uisub|
  	uisub.source_files = "Classes/ZSCategories/UIKit/UIColor/**/*.{h,m}"
  	uisub.dependency 'ZSCategories/YYCategories'
  end

  s.subspec 'UIControl' do |uisub|
  	uisub.source_files = "Classes/ZSCategories/UIKit/UIControl/**/*.{h,m}"
  	uisub.dependency 'ZSCategories/YYCategories'
  end

  s.subspec 'UIDeviceUIScreen' do |uisub|
  	uisub.source_files = "Classes/ZSCategories/UIKit/UIDeviceUIScreen/**/*.{h,m}"
  	uisub.dependency 'ZSCategories/YYCategories'
  end

  s.subspec 'UIFont' do |uisub|
  	uisub.source_files = "Classes/ZSCategories/UIKit/UIFont/**/*.{h,m}"
  	uisub.dependency 'ZSCategories/YYCategories'
  end

  s.subspec 'UIImage' do |uisub|
  	uisub.source_files = "Classes/ZSCategories/UIKit/UIImage/**/*.{h,m}"
  	uisub.dependency 'ZSCategories/YYCategories'
  end

  s.subspec 'UILabel' do |uisub|
  	uisub.source_files = "Classes/ZSCategories/UIKit/UILabel/**/*.{h,m}"
  end

  s.subspec 'UIScrollView' do |uisub|
  	uisub.source_files = "Classes/ZSCategories/UIKit/UIScrollView/**/*.{h,m}"
  	uisub.dependency 'ZSCategories/YYCategories'
  end

  s.subspec 'UITableView' do |uisub|
  	uisub.source_files = "Classes/ZSCategories/UIKit/UITableView/**/*.{h,m}"
  	uisub.dependency 'ZSCategories/YYCategories'
  end

  s.subspec 'UITextField' do |uisub|
  	uisub.source_files = "Classes/ZSCategories/UIKit/UITextField/**/*.{h,m}"
  end

  s.subspec 'UITextView' do |uisub|
  	uisub.source_files = "Classes/ZSCategories/UIKit/UITextView/**/*.{h,m}"
  end

  s.subspec 'UIView' do |uisub|
  	uisub.source_files = "Classes/ZSCategories/UIKit/UIView/**/*.{h,m}"
  	uisub.dependency 'ZSCategories/CALayer'
  end

  s.subspec 'UIKit' do |uikit|
  	uikit.dependency 'ZSCategories/UIViewController'
  	uikit.dependency 'ZSCategories/UIApplication'
  	uikit.dependency 'ZSCategories/UIBezierPath'
  	uikit.dependency 'ZSCategories/UIButton'
  	uikit.dependency 'ZSCategories/UIColor'
  	uikit.dependency 'ZSCategories/UIControl'
  	uikit.dependency 'ZSCategories/UIDeviceUIScreen'
  	uikit.dependency 'ZSCategories/UIFont'
  	uikit.dependency 'ZSCategories/UIImage'
  	uikit.dependency 'ZSCategories/UILabel'
  	uikit.dependency 'ZSCategories/UIScrollView'
  	uikit.dependency 'ZSCategories/UITableView'
  	uikit.dependency 'ZSCategories/UITextField'
  	uikit.dependency 'ZSCategories/UITextView'
  	uikit.dependency 'ZSCategories/UIView'
  end
  # ----------------  UIKit ----------------


  # ----------------  Quartz ----------------

  s.subspec 'CALayer' do |layer|
  	layer.source_files = "Classes/ZSCategories/Quartz/CALayer/**/*.{h,m}"
  	layer.dependency 'ZSCategories/YYCategories'
  end

  s.subspec 'Quartz' do |quartz|
  	quartz.dependency 'ZSCategories/CALayer'
  end
  # ----------------  Quartz ----------------
  

  # ――― Resources ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  A list of resources included with the Pod. These are copied into the
  #  target bundle with a build phase script. Anything else will be cleaned.
  #  You can preserve files from being cleaned, please don't preserve
  #  non-essential files like tests, examples and documentation.
  #

  # s.resource  = "icon.png"
  # s.resources = "Resources/*.png"

  # s.preserve_paths = "FilesToSave", "MoreFilesToSave"


  # ――― Project Linking ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  Link your library with frameworks, or libraries. Libraries do not include
  #  the lib prefix of their name.
  #

  # s.framework  = "SomeFramework"
  # s.frameworks = "SomeFramework", "AnotherFramework"

  # s.library   = "iconv"
  # s.libraries = "iconv", "xml2"


  # ――― Project Settings ――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  If your library depends on compiler flags you can set them in the xcconfig hash
  #  where they will only apply to your library. If you depend on other Podspecs
  #  you can include multiple dependencies to ensure it works.

  # s.requires_arc = true

  # s.xcconfig = { "HEADER_SEARCH_PATHS" => "$(SDKROOT)/usr/include/libxml2" }
  # s.dependency "JSONKit", "~> 1.4"

end
